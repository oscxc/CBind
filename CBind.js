function CBind(containerId) {
    'use strict';
    var data = {};
    var container = containerId&&containerId!=""?"#"+containerId:document.body.nodeName;
    function selector(s) {return document.querySelectorAll(s);}
    function type(v) {
        return Object.prototype.toString.apply(v).replace("[object ","").replace("]","");
    }
    function setTempList(domList,attrName) {
        var tempList={};
        for(var i=0;i<domList.length;i++){
            var value = domList[i].getAttribute(attrName);
            if(!tempList[value]){
                tempList[value] = [];
            }
            tempList[value].push(i);
        }
        return tempList;
    }
    var mvDomList = selector(container+" *[mv]");
    var vmDomList = selector(container+" *[vm]");
    var mvsDomList = selector(container+" *[mvs]");
    var mvTempList = setTempList(mvDomList,"mv");
    var vmTempList = setTempList(vmDomList,"vm");
    var mvsTempList = setTempList(mvsDomList,"mvs");
    function InitData(tempList,domList) {
        for(var key in tempList){
            if(tempList[key].length==1){
                var el = domList[tempList[key][0]];
                if(["text","password","textarea","email","file","url","number","tel","search","color","range","date","month","week","time","datetime-local",].indexOf(el.type)!=-1){
                    data[key]=el.value;
                }
                else if(el.type=="checkbox"){data[key] = el.checked;}
                else if(el.type=="select-one"){data[key] = el.options[el.selectedIndex].value;}
                else{data[key] = el.innerHTML;}
            }
            else{
                if(!data[key]){data[key] = {};}
                for(var i=0;i<tempList[key].length;i++){
                    var el = domList[tempList[key][i]];
                    if(["text","password","textarea","email","file","url","number","tel","search","color","range","date","month","week","time","datetime-local",].indexOf(el.type)!=-1){
                        data[key][i] = el.value;
                    }
                    else if(el.type=="checkbox"||el.type=="radio"){data[key][i] = el.checked;}
                    else if(el.type=="select-one"){data[key][i] = el.options[el.selectedIndex].value;}
                    else{data[key][i] = el.innerHTML;}
                }
            }
        }
    }
    var isFirstSet = true;
    function mvData() {
        for(var key in mvTempList){
            (function (key) {
                data[key]={};
                if(mvTempList[key].length==1) {
                    var el = mvDomList[mvTempList[key][0]];
                    Object.defineProperty(data, key, {
                        set: function (value) {
                            if (["text", "password", "textarea", "email", "url", "number", "tel", "search", "color", "range", "date", "month", "week", "time", "datetime-local",].indexOf(el.type) != -1) {
                                if (key != value) {
                                    key = value;
                                    el.value = value;
                                }
                            }
                            else if (el.type == "checkbox") {
                                if (key != value) {
                                    key = value;
                                    el.checked = value;
                                }
                            }
                            else if (el.type == "select-one") {
                                if (key != value) {
                                    key = value;
                                    for (var x = 0; x < el.options.length; x++) {
                                        if (el.options[x].value == value) {
                                            el.options[x].selected = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            else {if (key != value) {key = value;el.innerHTML = value;}}
                        },
                        get: function () {
                            return key;
                        }
                    });
                }
                else {
                    for (var i = 0; i < mvTempList[key].length; i++) {
                        (function (i) {
                            data[key][i] = {};
                            var el = mvDomList[mvTempList[key][i]];
                            Object.defineProperty(data[key], i, {
                                set: function (value) {
                                    if(["text","password","textarea","email","url","number","tel","search","color","range","date","month","week","time","datetime-local",].indexOf(el.type)!=-1) {
                                        if (!isFirstSet && i != value) {i = value;el.value = value;}
                                        else if (isFirstSet) {i = value;}
                                    }
                                    else if(el.type=="checkbox"||el.type=="radio"){
                                        if(!isFirstSet&&i!=value){i= value;el.checked = value;}
                                        else if(isFirstSet){i= value;}
                                    }
                                    else if(el.type=="select-one"){
                                        if(!isFirstSet&&i!=value){
                                            i = value;
                                            for(var x=0;x<el.options.length;x++){
                                                if(el.options[x].value==value){el.options[x].selected = true;break;}
                                            }
                                        }
                                        else if(isFirstSet){i= value;}
                                    }
                                    else{
                                        if (!isFirstSet && i != value) {i = value;el.innerHTML = value;}
                                        else if (isFirstSet) {i = value;}
                                    }
                                },
                                get: function () {
                                    return i;
                                }
                            });
                        }(i));
                    }
                }
            }(key));
        }
    }
    mvData();
    InitData(mvTempList,mvDomList);
    isFirstSet = false;
    
    function mvsData() {
        for(var key in mvsTempList){
            (function (key) {
                data[key]={};
                var els = [];
                for(var i=0;i<mvsTempList[key].length;i++){
                    els.push(mvsDomList[mvsTempList[key][i]]);
                }
                Object.defineProperty(data, key, {
                    set:function (value) {
                        if (key != value) {
                            key = value;
                            for(var i=0;i<els.length;i++) {
                                if (["text", "password", "textarea", "email", "url", "number", "tel", "search", "color", "range", "date", "month", "week", "time", "datetime-local",].indexOf(els[i].type) != -1) {
                                    els[i].value = value;
                                }
                                else if (els[i].type == "checkbox") {
                                    els[i].checked = value;
                                }
                                else if (els[i].type == "select-one") {
                                    for (var x = 0; x < els[i].options.length; x++) {
                                        if (els[i].options[x].value == value) {
                                            els[i].options[x].selected = true;
                                            break;
                                        }
                                    }
                                }
                                else{
                                    els[i].innerHTML = value;
                                }
                            }
                        }
                    },
                    get: function () {
                        return key;
                    }
                });
                if (["text", "password", "textarea", "email", "url", "number", "tel", "search", "color", "range", "date", "month", "week",
                        "time", "datetime-local",].indexOf(els[0].type) != -1) {
                    data[key] = els[0].value;
                }
                else if (els[0].type == "checkbox") {
                    data[key] = els[0].checked;
                }
                else if (els[0].type == "select-one") {
                    data[key] = els[0].options[els[0].selectedIndex].value;
                }
                else{
                    data[key] = els[0].innerHTML;
                }
            }(key));
        }
    }
    mvsData();

    function vmData() {
        for(var key in vmTempList){
            (function (key) {
                if(vmTempList[key].length==1){
                    var el = vmDomList[vmTempList[key][0]];
                    if(["text","password","textarea","email","url","number","tel","search","color","range","date","month","week","time","datetime-local",].indexOf(el.type)!=-1){
                        el.addEventListener("input", function () {data[key] = this.value;});
                    }
                    else if(el.type=="checkbox"){
                        el.addEventListener("change", function () {data[key] = this.checked;});
                    }
                    else if(el.type=="file"){
                        el.addEventListener("change", function () {data[key] = this.value;});
                    }
                    else if(el.type=="select-one"){
                        el.addEventListener("change", function () {data[key] = this.options[this.selectedIndex].value;});
                    }
                    else {
                        el.addEventListener("input", function () {data[key] = this.innerHTML;});
                    }
                }
                else{
                    for(var i=0;i<vmTempList[key].length;i++){
                        (function (i) {
                            var el = vmDomList[vmTempList[key][i]];
                            if(["text","password","textarea","email","url","number","tel","search","color","range","date","month","week","time","datetime-local",].indexOf(el.type)!=-1){
                                el.addEventListener("input", function () {data[key][i] = this.value;});
                            }
                            else if(el.type=="checkbox"){
                                el.addEventListener("change", function () {data[key][i] = this.checked;});
                            }
                            else if(el.type=="radio"){
                                el.addEventListener("click", function () {
                                    var radios = selector("input[type=radio][vm=" + this.getAttribute("vm") + "]");
                                    for(var j=0;j<radios.length;j++){data[key][j] = false;}
                                    data[key][i] = true;
                                });
                            }
                            else if(el.type=="file"){
                                el.addEventListener("change", function () {data[key][i] = this.value;});
                            }
                            else if(el.type=="select-one"){
                                el.addEventListener("change", function () {data[key][i] = this.options[this.selectedIndex].value;});
                            }
                            else{
                                el.addEventListener("input", function () {data[key][i] = this.innerHTML;});
                            }
                        }(i));
                    }
                }
            }(key));
        }
    }
    vmData();
    InitData(vmTempList,vmDomList);

    data.set = function (obj,async) {
        function setdata() {
            for(var key in obj){
                if(type(obj[key])!="Object"){
                    data[key] = obj[key];
                }
                else{
                    for(var _key in obj[key]){
                        data[key][_key] = obj[key][_key];
                    }
                }
            }
        }
        if(async==false){
            setdata();
        }
        else{
            setTimeout(setdata,0);
        }
    };
    data.get = function () {
        var obj = {};
        for(var key in data){
            if(type(data[key])!="Object"){
                obj[key] = data[key];
            }
            else{
                obj[key] = {};
                for(var _key in data[key]){
                    obj[key][_key] = data[key][_key];
                }
            }
        }
        delete obj["set"],delete obj["get"];
        return obj;
    };
    return data;
}